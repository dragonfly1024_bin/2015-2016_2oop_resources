#!/usr/bin/python
# -*- coding: utf-8 -*-


"""
	
"""


import os
import sys


class Compte:

	nombreCompte = 0									# Déclaration et initialisation d'un attribut de la classe compte.

	def crediter(self, x):
		self.solde += x
		
	def afficherSolde(self):
		print("\nLe solde vaut: %f" % self.solde)
		
	@classmethod
	def afficherNombreCompte(cls):						# Déclaration et définition d'une méthode de la classe compte.
		print("\nLe nombre de compte est: %d" % cls.nombreCompte)
	


def main():
	
	mon_compte = Compte()
	
	mon_compte.nom = "Dupont"
	mon_compte.identifiant = 130789
	mon_compte.solde = 100.5
	Compte.nombreCompte += 1							# Accèss et modification d'un attrubut de la classe.
	
	mon_compte.afficherSolde()
	Compte.afficherNombreCompte()						# Appelle à une méthode de class.
	
	
	return 0


if __name__ == "__main__":
	main()
