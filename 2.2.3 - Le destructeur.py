#!/usr/bin/python
# -*- coding: utf-8 -*-


"""
	
"""


import os
import sys


class Compte:
	
	nbrComptes = 0
	
	def __init__(self, nom="unknown", identifiant=0, solde=0.0):
		self.nom = nom
		self.identifiant = identifiant
		self.solde = solde
		Compte.nbrComptes += 1
		
	def __del__(self):													# Déclaration et définition du destructeur.
		Compte.nbrComptes -= 1											# La libération de mémoire se fait de façon implicite.
		
	def afficherSolde(self):
		print("\nLe solde vaut : %f" % self.solde)
		
	@classmethod
	def printNbrComptes(cls):
		print("\nLe nombre de compte est: %d" % cls.nbrComptes)


def main():
	
	mon_compte = Compte("Dupont")										# Ici Compte.nbrComptes vaut 1
	Compte.printNbrComptes()
	
	return 0															# Appelle implicite au destructeur.


if __name__ == "__main__":
	main()
	Compte.printNbrComptes()											# Ici Compte.nbrComptes vaut 0
