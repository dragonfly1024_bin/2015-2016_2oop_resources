#!/usr/bin/python
# -*- coding: utf-8 -*-


"""
	
"""


import os
import sys


class Compte:
	
	def __init__(self, nom="unknown", identifiant=0, solde=0.0):
		self.__nom = nom												# Pour préciser qu'un attribut est privé on précèdera le nom de l'attribut de deux underscore. La technique est la même pour les méthodes.
		self.__identifiant = identifiant
		self.__solde = solde
		
	def afficherSolde(self):
		print("\nLe solde vaut : %f" % self.__solde)


def main():
	
	mon_compte = Compte("Dupont")
	mon_compte.afficherSolde()
	
	print(mon_compte.__nom)												# Erreur, l'interprète crash. Comme l'attribut est privé on ne peux pas y accéder.
	
	return 0


if __name__ == "__main__":
	main()
